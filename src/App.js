import React, { Component } from 'react';
import axios from 'axios';
// import './styles/css/bootstrap.min.css';
// import './styles/css/bootstrap-rtl.min.css';
import Header from './Components/Header/Header';
import Footer from './Components/Footer/Footer';
import PrivateRoute from "./Components/PrivateRoute/PrivateRoute";
import { Route , Switch } from "react-router-dom";
import Home from "./Pages/Home/Home";
import NoMatch from "./Pages/NoMatch/NoMatch";
import Tutorial from "./Pages/Tutorial/Tutorial";
import Contactus from "./Pages/Contactus/Contactus";
import SingleTutorial from "./Pages/SingleTutorial/SingleTutorial";
import singleNews from  "./Pages/SingleNews/SingleNews";
import Newss from "./Pages/Newss/Newss";
import AboutUs from "./Pages/AboutUs/AboutUs";
import Tecnofund from "./Pages/Tecnofund/Tecnofund";
import SingleTecnofund from "./Pages/SingleTecnofund/SingleTecnofund";
import Login from "./Pages/Login/Login";
import PrimaryMarket from "./Pages/PrimaryMarket/PrimaryMarket";
import UserPanel from "./Pages/UserPanel/UserPanel";
import Shop from "./Pages/Shop/Shop";
import Product from "./Pages/Product/Product";
import ShopingCart from "./Pages/ShopingCart/ShopingCart";



import './'
import AccountReg from './Pages/AccountReg/AccountReg';
// import axiosInstance from "./axiosApi";


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isAuthenticated : true
    }
}

componentDidMount() {
    let apiToken = localStorage.getItem('token');

      if(apiToken !== null) {
        axios.post(`http://192.168.1.27:8010/api/jwt/verify/`, {'token': apiToken})
          .then(response => {
                  console.log(localStorage.getItem('apitoken', response.data.token));
                  this.setState({ isAuthenticated: true})
                  console.log(this.state.isAuthenticated);
              })
              .catch(error => {
                  this.setState({ isAuthenticated: false})
              })
      } else {
          this.setState({ isAuthenticated: false})
      }
  }
handleLogout() {
    localStorage.removeItem('apitoken');
    this.setState({ isAuthenticated : false});
}

handleLogin() {
    this.setState({ isAuthenticated : true});
}
handleSuccessfulAuth(){
    // this.props.handleLogin(data);
    this.props.history.push("/");
}
  render() {
    
    return (
    <React.Fragment>
     <Header auth={this.state.isAuthenticated} logout={this.handleLogout.bind(this)}/>

                   <Switch>
                       <Route path="/" exact={true} component={Home}/>
                       <Route path="/primarymarket" component={PrimaryMarket}/>
                       <Route path="/accountreg" component={AccountReg}/>
                       <Route path="/tutorials" component={Tutorial}/>
                       <Route path="/tutorial/:slug" component={SingleTutorial}/>
                       <Route path="/news/:slug" component={singleNews}/>             
                       <Route path="/contactus" component={Contactus}/>
                       <Route path="/news" component={Newss}/>
                       <Route path="/aboutus" component={AboutUs}/>
                       <Route path="/tecnofunds" component={Tecnofund}/>
                       <Route path="/tecnofund/:slug" component={SingleTecnofund}/>
                       <Route path="/shopingcart" component={ShopingCart}/>
                       <Route path="/shop" component={Shop}/>
                       <Route path="/product/:slug" component={Product}/>
                       <PrivateRoute path="/user-panel" component={UserPanel} auth={this.state.isAuthenticated}/>
                        <Route path="/login" render={(props) => <Login {...props} auth={this.state.isAuthenticated} login={this.handleLogin.bind(this)}/>}/>
                        <Route component={NoMatch}/>
                   </Switch>
       

           <Footer/>

    </React.Fragment>
  );
}
}

export default App;
