import React, { Component } from 'react'
import { array } from 'prop-types';

class Recently extends Component {
    constructor(props) {
        super(props);
 
        this.state = {
          isvalid :  Array.isArray(this.props),          
        }
    }
    

    render() {
        const {post} = this.props;
        return (
     
                    <div className="media post_item">
                       
                        <img src={this.state.isvalid ? post.pictures[0].picture : post.picture  } alt="post" />
     
                        <div className="media-body">
                            <a href="single-blog.html">
                                <h3>{post.title}</h3>
                            </a>
                            <div dangerouslySetInnerHTML={{ __html: post.summary.substr(0,60) }} />
                        </div>
                    </div>
              
        
        )
    }
}
export default Recently;
 
