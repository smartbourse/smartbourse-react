import React, { Component } from 'react'

class Search extends Component {
    render() {
        return (
       
                <aside className="single_sidebar_widget search_widget">
                    <form action="#">
                        <div className="form-group">
                            <div className="input-group mb-3">
                                <input type="text" className="form-control" placeholder="بنویس،  تا کمکت کنم ... "
                                    
                                />
                                <div className="input-group-append">
                                    <button className="btn" type="button"><i className="ti-search"></i></button>
                                </div>
                            </div>
                        </div>
                        <button className="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn"
                            type="submit">جستجو</button>
                    </form>
                </aside>
        
        )
    }
}
export default Search;
