import React, { Component } from 'react';
import {GetinDiv} from './style';
import axios from 'axios';
import Alerts from '../../Components/Alerts/Alerts';
import { Link } from 'react-router-dom';

 class Getin extends Component {
    constructor(props) {
        super(props) 

            this.state = {
                full_name: '',
                email: '',
                subject: '',
                message: '',
                from_where:'',
                successMessage:'',
                loading: false,
                error: ''
            }

    }




    onSubmit = e => {
        e.preventDefault()
        console.log(this.state);

      
        axios
          .post(
            `https://smartbourse.com/api/v1/contact/`,
            this.state
          )
          .then(response => {
            this.setState({loading: false, successMessage: 'Yay your message was sent'})
            console.warn( response.data)
            console.log(response)
          })
          .catch(error => {
            this.setState({ error: error.response.data, loading: false})
              console.log(error)
          })
      }
      

    
    handleOnChange = (event) => {
        event.preventDefault()
    
        this.setState({ [event.target.name]: event.target.value})
      
    
    }
    render() {
        const { full_name, email, subject, message, from_where,successMessage} = this.state
        return (
            <GetinDiv>
            <div data-scroll-index="" className="get_in_tauch_area">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-6">
                        <Link to="/contactus"> 
                            <div className="section_title text-center mb-90">
                                <h3 className="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">با ما در تماس باشید</h3>
                                <p className="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">مخاطب در این حوزه فعالیت مناسبی را در شبکه های اجتماعی تلگرام و اینستاگرام دنبال میکند</p>
                            </div>
                          </Link>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-lg-8">
                            <div className="touch_form">
                                <form method="post" id="contactForm" onSubmit={this.onSubmit}>
                                    <div className="row">
                                        {/* <div className="col-md-6">
                                            <div className="single_input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                                                <input type="text" name='full_name' value={full_name}  onChange={this.handleOnChange}  placeholder="نام و نام خانوادگی" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="single_input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">
                                                <input type="email" name='email' value={email}  onChange={this.handleOnChange} placeholder="ایمیل" />
                                            </div>
                                        </div> */}
                                        {/* <div className="col-md-12"> */}
                                            {/* <div className="single_input input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s"> */}
                                                {/* <label for="id_subject">از کجا با ما آشنا شدید</label> */}
                                                {/* <select name='from_where' value={from_where}  onChange={this.handleOnChange} id="subject" aria-required="true" className="form-control required name" required="" >
                                                    <option value="" defaultValue="" disabled>از کجا با ما آشنا شدید</option>
                                                    <option value="instagram">instagram</option>
                                                    <option value="google search">google search</option>
                                                    <option value="pinterest">Telegram</option>
                                                    <option value="from people">from people</option>
                                                </select>
                                            </div>
                                        </div> */}
                                        {/* <div className="col-md-12">
                                            <div className="single_input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s">
                                                <input type="text" name='subject' value={subject}  onChange={this.handleOnChange} placeholder="موضوع" />
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="single_input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">
                                            <textarea name='message' value={message}  onChange={this.handleOnChange} cols="30" placeholder="پیام خود را وارد کنید" rows="10"></textarea>
                                            </div>
                                        </div>
                                        
                                        <div className="col-lg-12">
                                            <div className="submit_btn wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s">
                                                <button className="boxed-btn3" type="submit">ارسال پیام </button>
                                            </div>
                                        </div> */}
                                    </div>
                                </form>
                            </div>
                            <div className="col-lg-12">
                            {successMessage  ? <Alerts  />:'' }
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            </GetinDiv>
        )
    }
}
export default Getin;