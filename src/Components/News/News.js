import React, { Component } from 'react';
import {NewsDiv} from './style';
import {NewsDetail} from './style';
import { Link } from 'react-router-dom';
import axios from 'axios';

class News1 extends Component {
    render(){
        const {post} = this.props;
       return(
            <div className={this.props.col}>
                <div className="single_Portfolio wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                    <div className="portfolio_thumb">
                        <img src={post.picture} alt="" />
                    </div>
                    <div className="portfolio_hover">
                        <div className="title">                                       
                                <h3>{post.title}</h3>
                                <span>در خصوص حداقل ارزش هر سفارش خريد ارسالي از طريق سامانه معاملات برخط کارگزاران، سهامداران و متقاضيانر.</span>
                                <Link to={`/aboutus`} className="boxed-btn3" >بیشتر</Link>
                        </div>
                    </div>
                </div>
            </div>



       ) 
    }
 }

 class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
            analysis : [],
            showItems: 5

        }
    }

    componentDidMount() {
        axios.get("https://smartbourse.com/api/v1/news/")
            .then(response => {
                const data = response.data.results;               
                this.setState({
                    analysis: data
                })

                console.log('Look here:', this.state.analysis);
            })
            .catch(error => {
                console.log(error);
            })

        }
    render() {
        const littleNews = this.state.analysis.slice(1, this.state.showItems).map((post , index) => <News1 col="col-lg-4 col-md-6" post={post} key={index}/>)
        const bigNews = this.state.analysis.slice(0,1).map((post , index) => <News1 col="col-lg-8 col-md-12" post={post} key={index}/>)
        return (
            <NewsDiv>
                <NewsDetail>
               
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-6">
                            <div className="section_title text-center mb-90">
                                <h3 className="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">اخبار</h3>
                                <p className="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">مجموعه اسمارت رویکرد جدید را با ارایه راهکارهای مبتنی بر هوش ماشین در معاملات سهام ارایه داده است</p>
                            </div>
                        </div>
                    </div>
                    <div className="row">


                        {bigNews}
                        {littleNews} 
                    </div>
                </div>
            
                </NewsDetail>
            </NewsDiv>
            )
    }
}

export default News;

  