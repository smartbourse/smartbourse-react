import React, { Component } from 'react';
import axios from 'axios';

class Content extends Component {
    // constructor(props){
    //     super(props);
    //     this.state = {
    //         singleNews: this.props.obj
    //     }
    // }
    // constructor(props) {
    //     super(props); 
    //     this.state = {
    //         singleNews: []
    //     }
    // }
    


    render() {
        
        const {post} = this.props;
        return (
            <div>
                <div className="single-post">
                    <div className="feature-img">
                    <img className="img-fluid"  alt="" />
                    </div>
                    <div className="ck_beautify">
                        <h2>                      
                        {this.props.title}
                        </h2>
                    </div>
                </div>
            </div>
        )
    }
}
export default Content;