import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Category extends Component {
    render() {
        const {post} = this.props;
        return (
        
            <li>
                <Link to={post.url} className="d-flex">
                    <p>{this.props.title}</p> 
                    <p>&nbsp;(۱۲)</p>
                </Link>
            </li>                      
            
        )
    }
}
export default Category;

