import React, { Component } from 'react';
import { FooterDiv } from './style';
import NavItem from "../../NavItem";
// import { Link } from 'react-router-dom';

class Footer extends Component {
    render() {
        return (
            <FooterDiv>
                <footer className="footer">
                    <div className="footer_top">
                        <div className="container-fluid p-0">
                            <div className="align-items-center no-gutters">
                                {/* <div className="col-lg-1 col-md-1">
                                <div className="footer_logo wow fadeInRight" data-wow-duration="1s" data-wow-delay=".3s">
                                    <NavItem to="/">
                                        <img src="img/header/LOGO.png" alt=""></img>
                                    </NavItem>
                                </div>
                            </div> */}
                                {/* <div className="col-xl-9 col-lg-9 col-md-9">
                                <div className="menu_links">                         
                                    <ul>
                                            <li className="active wow fadeInDown" data-wow-duration="1s" data-wow-delay=".2s"><NavItem to="/" >خانه</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".3s"><NavItem to="/tecnofund" > آموزش</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".4s"><NavItem to="/tutorials">تکنوفاند</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".5s"><NavItem to="/primarymarket">عرضه اولیه</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".6s"><NavItem to="/shop">آکادمی</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".7s"><NavItem to="/news">اخبار</NavItem></li>    
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".8s"><NavItem to="/contactus">تماس با ما</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".9s"><NavItem to="/aboutus">درباره با ما</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s"><NavItem to="/accountreg">افتتاح حساب آنلاین</NavItem></li>
                                            
                                    </ul>
                                </div>
                            </div>
                            <div className="col-xl-2 col-lg-2 col-md-12">
                                <div className="socail_links">
                                    <ul>
                                    
                                         <li><a className="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s" href="#"> <i className="fa fa-facebook"></i> </a></li> 
                                         <li><a className="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s" href="#"> <i className="fa fa-twitter"></i> </a></li> 
                                        <li><a href="https://www.instagram.com/smartbourse/?hl=en" className="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s"> <i className="fa fa-instagram"></i> </a></li>
                                         <li><a className="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s" href="#"> <i className="fa fa-google-plus"></i> </a></li> 
                                    </ul>
                                </div>
                            </div> */}

                                <div className="row" style={{margin: 0}}>
                                    <div className="ftr footer-links col-md-3">
                                        <ul>
                                            <h5>لینک‌های مرتبط</h5>
                                            <li className="active wow fadeInDown" data-wow-duration="1s" data-wow-delay=".2s"><NavItem to="/" >سوالات</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".4s"><NavItem to="/tutorials">درباره ما</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".5s"><NavItem to="/primarymarket">عرضه اولیه</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".3s"><NavItem to="/tecnofund" >سیاست ها و حریم خصوصی</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".6s"><a target="_blank" href="https://www.seo.ir/">سازمان بورس و اوراق بهادار</a></li>


                                        </ul>
                                    </div>

                                    <div className="ftr footer-help col-md-3">

                                        <ul>
                                            <h5>راهنما</h5>
                                            <li className="active wow fadeInDown" data-wow-duration="1s" data-wow-delay=".2s"><NavItem to="/" >ثبت نام</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".4s"><NavItem to="/tutorials">خرید و پرداخت</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".5s"><NavItem to="/primarymarket">احراز هویت</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".3s"><NavItem to="/tecnofund" >پشتیبانی</NavItem></li>
                                            <li className="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".7s"><NavItem to="/news">پیشنهادات و شکایات</NavItem></li>



                                        </ul>

                                    </div>

                                    <div className="ftr footer-contact col-md-3">

                                        <ul>
                                            <h5>تماس با ما</h5>
                                           
                                            <p><a href="tel:02188032432"><i className="fa fa-phone pl-3"></i>021-88032432</a></p>
                                            <p><a href="mail:info@smartbourse.com"><i className="fa fa-envelope pl-3"></i>info@smartbourse.com</a></p>
                                        
                                            <p className="text-white"><i className="fa fa-map-marker pl-3"></i>آدرس: <span>میدان ونک - خیابان ملاصدرا -خیابان شیراز شمالی-خیابان زاینده رود -پلاک4 -طبقه 4</span></p>
                                            <p className="text-white">با ما در شبکه های اجتماعی</p>
                                            <p>
                                                <a className="p-3" href="https://t.me/Nahayatnegar_broker"><i className="fa fa-telegram fa-lg"></i></a>
                                                <a className="p-3" href="https://www.linkedin.com/company/smart-bourse"><i className="fa fa-linkedin fa-lg"></i></a>
                                                <a className="p-3" href="https://www.instagram.com/smartbourse/?hl=en"><i className="fa fa-instagram fa-lg"></i></a>




                                            </p>



                                        </ul>

                                    </div>


                                    <div className="ftr footer-map col-md-3">

                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d809.4262175774122!2d51.40240782924656!3d35.75805808705659!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzXCsDQ1JzI5LjAiTiA1McKwMjQnMTAuNiJF!5e0!3m2!1sen!2sus!4v1590837334210!5m2!1sen!2sus"  allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

                                    </div>








                                </div>




                            </div>
                        </div>
                    </div>

                    <div className="copy-right_text">
                        <div className="container">
                            <div className="footer_border"></div>
                            <div className="row">
                                <div className="col-xl-12">
                                    <p className="copy_right text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
                                        2019 &copy;<script> </script> تمامی حق و حقوق این سایت محفوظ می باشد. <i className="fa fa-heart-o" aria-hidden="true"></i> توسط <a href="https://smartbourse.com" target="_blank">SmartBourse </a>

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </FooterDiv>
        )
    }
}
export default Footer;
