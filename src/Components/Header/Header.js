import React, { Component } from 'react';
import {HeaderMain} from './style';
import NavItem from "../../NavItem";
// import { Link } from 'react-router-dom';


 class Header extends Component {
    render() {
        const { auth : isAuthenticated } = this.props;
        return (
            <HeaderMain>
            <div className="header-area ">
                <div id="sticky-header" className="main-header-area">
                    <div className="container-fluid p-0">
                        <div className="row align-items-center no-gutters">
                            <div className="col-xl-1 col-md-1 col-lg-1 logo-left">
                                <div className="logo-img">
                                    <NavItem to="/">
                                        <img src="img/header/LOGO.png" alt="" />
                                    </NavItem>
                                </div>
                            </div>
                            <div className="col-xl-11 col-md-11 col-lg-11">
                                <div className="main-menu  d-none d-lg-block text-center">
                                    <nav>
                                        <ul id="navigation">
                                            <li  className="active" ><NavItem to="/" >خانه</NavItem></li>
                                            <li><NavItem to="/tecnofunds">تکنوفاند </NavItem></li>
                                            <li><NavItem to="/news">اخبار</NavItem></li>  
                                            <li><NavItem to="/primarymarket">عرضه اولیه</NavItem></li> 
                                            <li><NavItem to="/tutorials"> مقالات </NavItem></li>
                                     
                                            {/* <li><NavItem to="/shop">آکادمی</NavItem></li> */}
                                            <li><NavItem to="/shop">آکادمی <i class="ti-angle-down"></i></NavItem>
                                                <ul class="submenu">
                                                    <li><a href="portfolio_details.html">دوره جامع معامله گر هوشمند </a></li>                                           
                                                </ul>
                                            </li>
                                            <li><NavItem to="/accountreg">افتتاح حساب بورسی</NavItem></li>
                                            <li><NavItem to="/aboutus">درباره با ما</NavItem></li>
                                            <li><NavItem to="/contactus">تماس با ما</NavItem></li>
                                           
                                          
                                            <li>
                                                <NavItem to="/">
                                                {
                                                isAuthenticated
                                                    ? (
                                                        <div>
                                                            <NavItem className="btn btn-success say_hi" to="/user-panel"><i className="fa fa-user"  style={{ marginLeft :3}}></i>پنل کاربری</NavItem>
                                                        
                                                            <button className="btn btn-danger" style={{ marginRight : 10}} onClick={this.props.logout}>خروج</button>
                                                            
                                                        </div>
                                                    ) : (
                                                        <div  className="say_hi">
                                                            <NavItem to="/login">ورود / عضویت</NavItem>
                                                        </div>
                                                    )
                                            }
                                                </NavItem>

                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            {/* <div className="col-lg-2 col-md-2 d-none d-lg-block">
                                <div className="log_chat_area d-flex align-items-end">
                                   
                                   <NavItem to="/shopingcart"><i className="fa fa-shopping-basket"  style={{marginLeft : 20}}><i className="fa fa-circle"></i></i></NavItem>
                                   <div >
                                                                    {
                                isAuthenticated
                                    ? (
                                        <div>
                                            <NavItem className="btn btn-success say_hi" to="/user-panel"><i className="fa fa-user"  style={{ marginLeft :3}}></i>پنل کاربری</NavItem>
                                           
                                            <button className="btn btn-danger" style={{ marginRight : 10}} onClick={this.props.logout}>خروج</button>
                                            
                                        </div>
                                    ) : (
                                        <div  className="say_hi">
                                            <NavItem to="/login">ورود / عضویت</NavItem>
                                        </div>
                                    )
                            }
                                    </div>
                                </div>
                            </div> */}
                            <div className="col-12">
                                <div className="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        </HeaderMain>
            )
    }
}

export default Header;