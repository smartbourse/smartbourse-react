import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Blog extends Component {
    constructor(props) {
        super(props);
 
        this.state = {
          isvalid :  Array.isArray(this.props.post.pictures), 
        }
    }

    render() {
        const {post} = this.props;
        return (
            
            <div className={this.props.col} style={{float:"right"}}>
                <div className="blog_left_sidebar">
                <div>{ console.log(this.state.isvalid) }</div>
                    <Link to={`${this.props.page_path}/${post.slug}`}>
                    <article className="blog_item">
                        <div className="blog_item_img">
                            <img className="card-img rounded-0" src={this.state.isvalid ? post.pictures[0].picture : post.picture } alt="" />
                                <Link to={`${this.props.page_path}/${post.slug}`} className="blog_item_date">
                                <h3>۱۵</h3>
                                <p>فروردین</p>
                                </Link>
                        </div>

                        <div className="blog_details">
                                <Link to={`${this.props.page_path}/${post.slug}`} className="d-inline-block" >
                                <h2>{post.title}</h2>
                                </Link>
                                <p dangerouslySetInnerHTML={{ __html: post.summary.substr(0,200) }} />
                            <ul className="blog-info-link">
                                <li><Link to={`${this.props.page_path}/${post.slug}`}><i className="fa fa-angle-left"></i> بیشتر بدانید ...</Link></li>
                            </ul>
                        </div>
                    </article>
                    </Link>
                </div>
            </div>

        )
    }
}
export default Blog;
