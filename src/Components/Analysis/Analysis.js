import React, { Component } from 'react';
import axios from 'axios';
import {AnalysisDiv} from './style';
class AnalysBlog extends Component {
    render(){
        const {post} = this.props;
       return(

        <div className="col-lg-2 col-md-4">
            <div className="single_team wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s">
                <div className="team_thumb">
                    <img src={post.picture} alt="" />
                    <div className="team_hover">
                        <div className="hover_inner text-center">
                        </div>
                    </div>
                </div>
                <div className="team_title text-center">
                    <h3>{post.title}</h3>
                    <p>{post.summary}</p>
                </div>
            </div>
        </div>

       ) 
    }
 }
class Analysis extends Component {
    constructor(props) {
        super(props);
        this.state = {
            analysis : [],
            showItems: 6

        }
    }

    componentDidMount() {
        axios.get("https://smartbourse.com/api/v1/analysis/")
            .then(response => {
                const data = response.data.results;               
                this.setState({
                    analysis: data
                })

                console.log('Look here:', this.state.analysis);
            })
            .catch(error => {
                console.log(error);
            })

        }
    render() {
        const items = this.state.analysis.slice(0, this.state.showItems).map((post , index) => <AnalysBlog post={post} key={index}/>)
        return (
            <AnalysisDiv>
            <div className="team_area ">
                <div className="container-fluid">
                    <div className="row justify-content-center">
                        <div className="col-lg-6">
                            <div className="section_title text-center mb-90">
                                <h3 className="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s"> تکنوفاند </h3>
                                <p className="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s">پاسار شکست سقف و شکل گیری سقفی بالاتر از سقف قبلی با شکست</p>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        {items} 
                    </div>
                </div>
            </div>
            </AnalysisDiv>
        )
    }
}
export default Analysis;
