import React, { useState } from 'react';
import { Alert } from 'reactstrap';

const Alerts = (props) => {
  const [visible, setVisible] = useState(true);

  const onDismiss = () => setVisible(false);

  return (
    <Alert  isOpen={visible} toggle={onDismiss} style={{marginTop:20}}>
.پیام شما با موفقیت ارسال شد
    </Alert>
  );
}

export default Alerts;