import React, { Component } from 'react';
import Banner from '../../Components/Banner/Banner';
import { Shopdiv } from './style';
import axios from 'axios';
// import primage from './pr.jpg';
import { Link } from 'react-router-dom';
// import Blog from '../../Components/Blog/Blog';

 class Card extends Component {
    render(){
        const {post} = this.props;
       return(
        //
                <div className="col-md-4 text-center" style={{float:'right'}}>
                    <div className="spacer product-cart">
                    <Link to={`${this.props.page_path}/${post.slug}`}>
                    <img className="product-image" alt="smartbourse" src={post.pictures} />
                    </Link>
                    <div className="row py-3">
                        <div className="col-md-6 product-name">{this.props.title}</div>
                        <div className="col-md-6 product-price"><span>20,000,000</span><span> ریال</span></div>
                    </div>
                    <hr/>
                    <div className="row">
                        <div className="col-md-6 add-to-card"><button>افزودن به سبد</button></div>
                        <div className="col-md-6 QNT">تعداد:<input className="QNTinput text-center" type="number" min="0"/></div>
                    </div>
                    <Link to={`${this.props.page_path}/${post.slug}`}>
                    <p className="product-category text-right my-2 px-1">دسته بندی : <span>آموزش مقدماتی</span></p>
                    <p className="product-discription">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک است، چاپگرها و متون</p>
                    </Link>
                    </div>  
                </div>   

       )
    }
 }
class Shop extends Component {
    constructor(props) {
        super(props);
        this.state = {       
            products : [],          
        }
    }

    componentDidMount() {
        axios.get("https://smartbourse.com/api/v1/shop/products/")
            .then(response => {
                const data = response.data.results;               
                this.setState({
                    products: data
                })

                console.log('Look here:', this.state.products);
            })
            .catch(error => {
                console.log(error);
            })

            
        }

    render() {
        return (
            <React.Fragment>
                <Banner />
                <Shopdiv className="container">

                    <div className="row">
        
                         {this.state.products.map((post , index) => <Card  page_path="product" post={post} key={index}/>)} 
                    
                    </div>

                </Shopdiv>

            </React.Fragment>
        );
    }
}
export default Shop;
