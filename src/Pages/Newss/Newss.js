import React, { Component } from 'react';
import {NewsDiv } from './style';
import axios from 'axios';
import Blog from '../../Components/Blog/Blog';
import Banner from '../../Components/Banner/Banner';

class Newss extends Component {
    constructor(props) {
        super(props);
        this.state = {
            news : []
        }
    }

    componentDidMount() {
        axios.get("https://smartbourse.com/api/v1/news/")
            .then(response => {
                const data = response.data.results;             
                this.setState({
                    news: data
                })

                console.log('Look here:', this.state.news);
            })
            .catch(error => {
                console.log(error);
            })
        }


    render() {
        return (
            <React.Fragment>
            <Banner />
            <NewsDiv>
                 <section className="blog_area section-padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 mb-5 mb-lg-0">
                                <div className="col-lg-12">
                                    {this.state.news.map((post , index) => <Blog page_path="news" col="col-lg-4" post={post} key={index}/>)}
                                 </div>
                            </div>
                        
                        </div>
                    </div>
                </section>
            </NewsDiv>
            </React.Fragment>
        )
    }
}
export default Newss;