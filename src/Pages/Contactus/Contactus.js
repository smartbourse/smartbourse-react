import React, { Component } from 'react';
import {ContactDiv } from './style';
import Banner from '../../Components/Banner/Banner';
import axios from 'axios';
import Alerts from '../../Components/Alerts/Alerts';


 class Contactus extends Component {
    constructor(props) {
        super(props) 

            this.state = {
                full_name: '',
                email: '',
                subject: '',
                message: '',
                from_where:'',
                successMessage:'',
                loading: false,
                error: ''
            }

    }




    onSubmit = e => {
        e.preventDefault()
        console.log(this.state);

      
        axios
          .post(
            `https://smartbourse.com/api/v1/contact/`,
            this.state
          )
          .then(response => {
            this.setState({loading: false, successMessage: 'Yay your message was sent'})
            console.warn( response.data)
            console.log(response)
          })
          .catch(error => {
            this.setState({ error: error.response.data, loading: false})
              console.log(error)
          })
      }
      

    
    handleOnChange = (event) => {
        event.preventDefault()
    
        this.setState({ [event.target.name]: event.target.value})
      
    
    }
    render() {
        const { full_name, email, subject, message, from_where,successMessage} = this.state
        return (
            <React.Fragment>
            <Banner />
        
                <ContactDiv>
                  <section className="contact-section section_padding">
                    <div className="container">
                    <div className="d-none d-sm-block mb-5 pb-4">
                    <iframe title="smartboursetitle" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d481.2875550425989!2d51.40290172322176!3d35.75808345729287!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8e06ee45992df3%3A0xbf50d16807de538d!2z2YfZhNiv24zZhtqvINi52YXYsdin2YYg2Ygg2LPYp9iu2KrZhdin2YYg2KrYsdin2LIg2b7bjCDYsduM2LIgKCDYtdmG2K_ZiNmCINio2KfYstmG2LTYs9iq2q_bjCDaqdi02YjYsduMICk!5e0!3m2!1sen!2sca!4v1588599870802!5m2!1sen!2sca" width="1100" height="400" frameBorder="0" style={{border:"0"}}  aria-hidden="false" ></iframe>
                 
                    </div>


                    <div className="row">
                        <div className="col-12">
                        <h2 className="contact-title">تماس با ما</h2>
                        </div>
                        <div className="col-lg-4">
                        <div className="media contact-info">
                            <span className="contact-info__icon"><i className="ti-home"></i></span>
                            <div className="media-body">
                            <h3>تهران ,ونک, ملاصدرا</h3>
                            <p>خ شیراز شمالی , خ زاینده رود غربی ,پلاک ۴ , طبقه ۴</p>
                            </div>
                        </div>
                        <div className="media contact-info">
                            <span className="contact-info__icon"><i className="ti-tablet"></i></span>
                            <div className="media-body">
                            <h3>۲۴۳۲ ۸۸۰۳</h3>
                            <p>شنبه تا چهارشنبه </p>                      
                            </div>
                        </div>
                        <div className="media contact-info">
                            <span className="contact-info__icon"><i className="ti-email"></i></span>
                            <div className="media-body">
                            <h3>smartbourse@gmail.com</h3>
                            <p>در هر زمان درخواست خود را برای ما ارسال کنید!</p>
                            </div>
                        </div>
                        </div>
                  
                        <div className="col-lg-8">
                        <form className="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" onSubmit={this.onSubmit}>
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <input className="form-control"  name='full_name' value={full_name}  onChange={this.handleOnChange} id="name" type="text" placeholder = 'نام خود را وارد کنید' />
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <input className="form-control"  name='email' value={email}  onChange={this.handleOnChange} id="email" type="email"  placeholder = 'ایمیل خود را وارد کنید' />
                                    </div>
                                </div>
                                <div className="col-12">
                                    <div className="form-group">
                                        <input className="form-control"  name='subject' value={subject}  onChange={this.handleOnChange} id="subject" type="text"  placeholder = 'موضوع را وارد کنید' />
                                    </div>
                                </div>
                                {/* <div className="col-12">
                                    <div className="form-group">
                                        <input className="form-control"  name='from_where' value={from_where}  onChange={this.handleOnChange} id="subject" type="text"  placeholder = 'از کجا با ما آشنا شدید' />
                                    </div>
                                </div> */}
                                <div className="col-12">
                                    <div className="form-group">
                                    {/* <label for="id_subject">از کجا با ما آشنا شدید</label> */}
                                    <select name='from_where' value={from_where}  onChange={this.handleOnChange} id="subject" aria-required="true" class="form-control required name" required="" >
                                        <option value="" selected="" disabled>از کجا با ما آشنا شدید</option>
                                        <option value="instagram">instagram</option>
                                        <option value="google search">google search</option>
                                        <option value="pinterest">Telegram</option>
                                        <option value="from people">from people</option>
                                    </select>
                                </div>
                                </div>
                                <div className="col-12">
                                    <div className="form-group">
                                        <textarea className="form-control w-100" name='message' value={message}  onChange={this.handleOnChange} id="message" cols="30" rows="9"  placeholder = 'پیام خود را وارد کنید'></textarea>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group mt-3">
                            <button type="submit" className="button button-contactForm btn_4 boxed-btn">  ارسال پیام</button>                           
                            </div>

                         
                        </form>
                        </div>
                        <div className="col-lg-12">
                            {successMessage  ? <Alerts  />:'' }
                        </div>
                    </div>
                    </div>
                </section>
                </ContactDiv>

            </React.Fragment>
        )
    }
}

export default Contactus;