import styled from 'styled-components';
import {ravi_light} from '../../Components/variables/Font';
import {  mobile_device , tablet_device} from '../../Components/variables/responsive';
import {border_radius,transition} from '../../Components/variables/mixin';





export const AboutUsDiv = styled.div`

.portfolio_details_banner{
    background: #1F1F1F;
    padding-top: 220px;
    padding-bottom: 470px;
    @media ${mobile_device}{
        padding: 100px 0;
    }
    .details_info{
        h3{
            font-size: 45px;
            font-weight: 400;
            color: #fff;
            text-align: center;
            margin-bottom: 62px;
            @media ${mobile_device} {
                font-size: 30px;
                margin-bottom:6px;
            }
        }
        .details_links{
            @media ${mobile_device} {
                display: block !important;
                text-align: center;
            }
            .single_details{
                @media ${mobile_device}{
                    margin-bottom: 30px;
                }
                span{
                    color: #C9C9C9;
                    font-size: 12px;
                    font-weight: 400;
                    display: block;
                    margin-bottom: 9px;
                    font-family: ${ravi_light};
                }
                h4{
                    font-family: ${ravi_light};
                    font-size: 16px;
                    font-weight: 400;
                    color: #fff;
                }
                .social_links{
                    ul{
                        li{
                            display: inline-block;
                            margin-right: 21px;
                            @media ${mobile_device}{
                                margin: 0 5px;
                            }
                            a{
                                color: #fff;
                                font-size: 16px;

                                &:hover{
                                    color: #615CFD;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

.portfolio_details_wrap{
    padding-bottom: 130px;
    position: relative;
    margin-top: -380px;
    @media ${mobile_device}{
        padding-bottom: 60px;
    }
    @media ${mobile_device} {
        margin-top: 60px;
    }
    .banner{
        img{
            width: 100%;
        }
    }
    .details_text{
        margin-bottom: 70px;
        margin-top: 80px;
        @media ${mobile_device} {
            margin: 30px 0;
        }
        p{

        }
    }
    .thumbs{
        .single_thumb{
            margin-bottom: 30px;
            img{
                width: 100%;
            }
        }
    }
}
  .boxed-btn {
    float: left;
    background: #fff;
    color: #131313;
    display: inline-block;
    padding: 14px 44px;
    font-family: ${ravi_light};
    font-size: 16px;
    font-weight: 400;
    border: 0;
    border: 1px solid #615CFD;
    // width: 180px;
    text-align: center;
    color: #615CFD !important;
    text-transform: uppercase;
    cursor: pointer;
    &:hover{
        background: #615CFD;
        color: #fff !important;
        border: 1px solid #615CFD;
    }
    &:focus{
        outline: none;
    }
    &.large-width{
        width: 220px;
    }
} 
.our_mission_area{
    padding-bottom: 137px;
    @media ${mobile_device} {
        padding-top: 60px;
        padding-bottom: 30px;
    }
    @media ${tablet_device} {
        padding-top: 100px;
        padding-bottom: 70px;
    }
    .mission_text{
        h4{
            font-size: 24px;
            color: #615CFD;
            font-weight: 400;
            font-family: ${ravi_light};
        } 
        h3{
            font-size: 18px;
            color: #615CFD;
            font-weight: 400;
            font-family: ${ravi_light};
        }  
    }
    .mision_info{
        .single_mission{
            margin-bottom: 15px;
            p{
                color: #707070;
                font-size: 16px;
            }
        }
    }
}
.boxed-btn3 {
        /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#0181f5+0,5db2ff+100 */
    background: #615CFD;
	color: #fff;
	display: inline-block;
	padding: 12px 33px;
	font-family: ${ravi_light};
	font-size: 16px;
    font-weight: 500;
    border: 0;
    border: 1px solid transparent;
    ${border_radius('30px')};
    text-align: center;
    color: #fff !important;
    text-transform: capitalize;
    ${transition('.5s')};
    cursor: pointer;
    &:hover{
        border: 1px solid #615CFD;
        color: #615CFD !important;
        background: transparent ;
    }
    &:focus{
        outline: none;
    }
    &.large-width{
        width: 220px;
    }
}

  `;

