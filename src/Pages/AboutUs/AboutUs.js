import React, { Component } from 'react'
import {AboutUsDiv } from './style';


class AboutUs extends Component {
    render() {
        return (
            <React.Fragment>
                <AboutUsDiv>
                <div class="portfolio_details_banner">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-10">
                                <div class="details_info ">
                                    <h3 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".3s">درباره ما بیشتر بدانید . . .</h3>
                                    <div class="details_links d-flex justify-content-between align-items-center">


                                    </div>
                                </div>
                                {/* <span className="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">به اسمارت بورس خوش آمدید</span>
                                <h3 className="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">به اسمارت بورس خوش آمدی</h3> */}
                            </div>
                        </div>
                    </div>
                </div>
                
                   <div className="portfolio_details_wrap">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-lg-10">
                                <div className="banner wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                                    <img src="img/portfolio_details/details_banner.png" alt="" />
                                </div>
                                <div className="details_text">
                                    <div className="row">
                                        <div className="col-lg-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                                            <p>مجموعه اسمارت بورس در زمینه پیاده سازبگارگیری نیروهای متخصص از دانشگاههای برتر کشور حرکت سازنده، مبتنی بر دانش روز دنیا و تجربیات ارزشمند از بازار ایران شروع شده ی Machine Learning، هوش مصنوعی و الگوریتم تریدینگ، توسعه و معرفی بازار سهام و آموزش فعال است </p>
                                        </div>
                                        <div className="col-lg-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                                            <p>و در حال رسیدن به نتایج درخشان در راستای معاملات مبتنی بر ماشین با بازدهی مستمر و بالا می باشد توسعه بازار سهام با هدف معرفی این بازار پرسود به عموم جامعه از دیگر فعالیتهای مجموعه است بدیهی استدر حوزه Machine learning و هوش مصنوعی با بگارگیری نیروهای متخصص از دانشگاههای برتر کشور حرکت سازنده، مبتنی بر دانش روز دنیا و تجربیات ارزشمند از بازار ایران شروع شده</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="thumbs">
                                    <div className="row">
                                        <div className="col-lg-6 col-md-6">
                                            <div className="single_thumb wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".4s">
                                                <img src="img/portfolio_details/1.png" alt="" />
                                            </div>
                                        </div>
                                        <div className="col-lg-6 col-md-6">
                                            <div className="single_thumb wow fadeInRight" data-wow-duration="1s" data-wow-delay=".4s">
                                                <img src="img/portfolio_details/2.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="our_mission_area">
        <div className="container">
            <div className="row">
                <div className="col-lg-2">
                    <div className="mission_text">
                        <h4>ماموریت ما ...</h4>
                        <h3> جهت معرفی درست این بازار پرسود آموزش اجتناب ناپذیر است </h3> 
                    </div>
                </div>
                <div className="col-lg-10">
                    <div className="mision_info">
                        <div className="single_mission">
                        </div>
                        <div className="single_mission">
                            <p>مجموعه اسمارت بورس در زمینه پیاده سازی Machine Learning، هوش مصنوعی و الگوریتم تریدینگ، توسعه و معرفی بازار سهام و آموزش فعال است </p>
                        </div>
                        <div className="single_mission">
                            <p>در حو و در حال رسیدن به نتایج درخشان در راستای معاملات مبتنی بر ماشین با بازدهی مستمر و بالا می باشد توسعه بازار سهام با هدف معرفی این بازار پرسود به عموم جامعه از دیگر فعالیتهای مجموعه است بدیهی استزه Machine learning و هوش مصنوعی با بگارگیری نیروهای متخصص از دانشگاههای برتر کشور حرکت سازنده، مبتنی بر دانش روز دنیا و تجربیات ارزشمند از بازار ایران شروع شده</p>
                        </div>
                        <div className="single_mission">
                            <p>در حو و در حال رسیدن به نتایج درخشان در راستای معاملات مبتنی بر ماشین با بازدهی مستمر و بالا می باشد توسعه بازار سهام با هدف معرفی این بازار پرسود به عموم جامعه از دیگر فعالیتهای مجموعه است بدیهی استزه Machine learning و هوش مصنوعی با بگارگیری نیروهای متخصص از دانشگاههای برتر کشور حرکت سازنده، مبتنی بر دانش روز دنیا و تجربیات ارزشمند از بازار ایران شروع شده</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

                </AboutUsDiv>
            </React.Fragment>
        )
    }
}
export default AboutUs;