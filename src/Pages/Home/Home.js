import React, { Component } from 'react';


import Slider from '../../Components/Slider/Slider';
import Services from '../../Components/Services/Services';
import About from '../../Components/About/About';
import News from '../../Components/News/News';
import WeWork from '../../Components/WeWork/WeWork';
import Analysis from '../../Components/Analysis/Analysis';
import Getin from '../../Components/Getin/Getin';
// import Contacts from '../../Components/Contacts';
// import Subscribe from '../../Components/Subscribe/Subscribe';




class Home extends Component {
    // constructor() {
    //     super()
    //     this.state = {
    //         contacts: []
    //       }
    // }
    state ={
        loading:true,
        person:null,
    };

    //   componentDidMount() {
    //     fetch('http://localhost:8000/api/v1/api/v1/analysis/category/')
    //     .then(res => res.json())
    //     .then((data) => {
    //       this.setState({ contacts: data })
    //     })
    //     .catch(console.log)
    //   }
    // async componentDidMount(){
    //     const url = "https://smartbourse.com/api/v1/analysis/category/";
    //     const response = await fetch(url);
    //     const data = await response.json();
    //     this.setState({person: data.results[0]})
    //     console.log(data.results[0]);

    // }

    render() {
        return (
            <React.Fragment>
                <Slider />              
                    {/* {this.state.loading || this.state.person ? <div>loading...</div>: (<div>person...</div>)} */}
                <About />        
                <Services />
                <WeWork/> 
                <News />                           
                <Analysis />
                <Getin/>

          </React.Fragment>
        );
    }
}

export default Home;
