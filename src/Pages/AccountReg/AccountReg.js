import React, { Component } from 'react';
import { AccountRegDiv } from './style';
import axios from 'axios';
import Banner from '../../Components/Banner/Banner';
import Alerts from '../../Components/Alerts/Alerts'

class AccountReg extends Component {
    constructor(props) {
        super(props) 

            this.state = {
                full_name: '',
                national_code: '',
                phone: '',
                investment: '',
                familiarity:'',
                successMessage:'',
                loading: false,
                error: ''
            }

    }




    onSubmit = e => {
        e.preventDefault()
        console.log(this.state);

      
        axios
          .post(
            `https://smartbourse.com/api/v1/openaccount/`,
            this.state
          )
          .then(response => {
            this.setState({loading: false, successMessage: 'Yay your message was sent'})
            console.warn( response.data)
            console.log(response)
          })
          .catch(error => {
            this.setState({ error: error.response.data, loading: false})
              console.log(error)
          })
      }
      

    
    handleOnChange = (event) => {
        event.preventDefault()
    
        this.setState({ [event.target.name]: event.target.value})
      
    
    }
    render() {
        const { full_name, national_code, phone, investment, familiarity,successMessage} = this.state
        return (
            <React.Fragment>
                <Banner />
                <AccountRegDiv>
                {/* <div className="container"> */}
                             {/* <div className="row justify-content-center">
                                <h3 class="text-uppercase">افتتاح حساب</h3>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <h3 class="mb-30">Form Element</h3>
                                        <form onSubmit={this.onSubmit}>
                                            <div class="mt-10">
                                                <input type="text" name="first_name" placeholder="First Name"
                                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'" required
                                                    class="single-input" />
                                            </div>
                                            <div class="mt-10">
                                                <input type="text" name="last_name" placeholder="Last Name"
                                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'" required
                                                    class="single-input" />
                                            </div>
                                            <div class="mt-10">
                                                <input type="text" name="last_name" placeholder="Last Name"
                                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'" required
                                                    class="single-input" />
                                            </div>
                                            <div class="mt-10">
                                                <input type="email" name="EMAIL" placeholder="Email address"
                                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email address'" required
                                                    class="single-input" />
                                            </div>
                                            <div class="input-group-icon mt-10">
                                                <div class="icon"><i class="fa fa-thumb-tack" aria-hidden="true"></i></div>
                                                <input type="text" name="address" placeholder="Address" onfocus="this.placeholder = ''"
                                                    onblur="this.placeholder = 'Address'" required class="single-input" />
                                            </div>
                                            <div class="input-group-icon mt-10">
                                                <div class="icon"><i class="fa fa-plane" aria-hidden="true"></i></div>
                                                <div class="form-select" id="default-select">
                                                <select>
                                                    <option value=" 1">City</option>
                                                    <option value="1">Dhaka</option>
                                                    <option value="1">Dilli</option>
                                                    <option value="1">Newyork</option>
                                                    <option value="1">Islamabad</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="input-group-icon mt-10">
                                                <div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>
                                                <div class="form-select" id="default-select">
                                                <select>
                                                    <option value=" 1">Country</option>
                                                    <option value="1">Bangladesh</option>
                                                    <option value="1">India</option>
                                                    <option value="1">England</option>
                                                    <option value="1">Srilanka</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="mt-10">
                                                <textarea class="single-textarea" placeholder="Message" onfocus="this.placeholder = ''"
                                                    onblur="this.placeholder = 'Message'" required></textarea>
                                            </div>
                                        
                                        <div class="mt-10">
                                                <div class="primary-input">
                                                    <input id="primary-input" type="text" name="first_name" placeholder="نام خود را وارد نمایید" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Primary color'" />
                                                    <label for="primary-input"></label>
                                                </div>
                                            </div> 
                                            <div class="mt-10">
                                                <input type="text" name="full_name" value={full_name}  onChange={this.handleOnChange} placeholder="شماره تماس خود را وارد نمایید"
                                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Primary color'" required
                                                    class="single-input-primary" />
                                            </div>
                                            <div class="mt-10">
                                                <input type="text" name="phone" value={phone}  onChange={this.handleOnChange}  placeholder="Accent color"
                                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Accent color'" required
                                                    class="single-input-accent" />
                                            </div>
                                            <div class="mt-10">
                                                <input type="text" name="national_code" value={national_code}  onChange={this.handleOnChange} placeholder="کد ملی خود را وارد نمایید"
                                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Secondary color'"
                                                    required class="single-input-secondary" />
                                            </div>
                                            <div class="input-group-icon mt-10">
                                                <div class="icon"><i class="fa fa-plane" aria-hidden="true"></i></div>
                                                <div class="form-select" id="default-select">
                                                <label for="id_from_where">میزان سرمایه فعال مورد نظر شما برای سرمایه گذاری در بورس</label>
                                                <select name="investment"  value={investment}  onChange={this.handleOnChange}>
                                                <option value="under 100 milion">زیر 100 میلیون تومان</option>
                                                    <option value="between 100 to 500 milion">از 100 تا 500 میلیون تومان</option>
                                                    <option value="between 500 milion to 1 miliard">از 500 میلیون تا یک میلیارد تومان</option>
                                                    <option value="between 1 to 5 miliard">از 1 تا 5 میلیارد تومان</option>
                                                    <option value="between 5 to 10 miliard">از 5 تا 10 میلیارد توامن</option>
                                                    <option value="above 10 miliard">بیش از 10 میلیارد تومان</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="input-group-icon mt-10">
                                                <div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>
                                                <div class="form-select" id="default-select">
                                                <label for="id_subject">میزان آشنایی شما با بازار بورس چقدر است</label>
                                                <select ame="familiarity" value={familiarity}  onChange={this.handleOnChange}>
                                                <option value="" selected="">---------</option>
                                                    <option value="low">کم</option>
                                                    <option value="Moderate">متوسط</option>
                                                    <option value="well">خوب</option>
                                                    <option value="high">عالی</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div> */}
                                <div class="m-t-30">
                                    <form class="widget-contact-form rtl" action="/open-account/" role="form" method="post" onSubmit={this.onSubmit}>
                                        <input type="hidden" name="csrfmiddlewaretoken" value="ngCaUlCGhjFnUXZHa8cSB8oqAItKq3786vpTKeEstDxyqtmySidI57Rt7KFvfyGt" />
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="id_full_name">نام و نام خانوادگی</label>
                                                <input type="text" name="full_name" value={full_name}  onChange={this.handleOnChange} maxlength="100" aria-required="true" class="form-control required name" required="" placeholder="نام خود را وارد نمایید" id="id_full_name" />
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="id_email">شماره تماس</label>
                                                <input type="text" name="phone" value={phone}  onChange={this.handleOnChange} maxlength="10" aria-required="true" class="form-control required name" required="" placeholder="شماره تماس خود را وارد نمایید" id="id_phone" />
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="id_email">کد ملی</label>
                                                <input type="text" name="national_code" value={national_code}  onChange={this.handleOnChange} maxlength="10" aria-required="true" class="form-control required name" required="" placeholder="کد ملی خود را وارد نمایید" id="id_national_code" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <label for="id_from_where">میزان سرمایه فعال مورد نظر شما برای سرمایه گذاری در بورس</label>
                                                <select name="investment"  value={investment}  onChange={this.handleOnChange} aria-required="true" class="form-control required name" required="" id="id_investment">
                                                    <option value="" selected="">---------</option>
                                                    <option value="under 100 milion">زیر 100 میلیون تومان</option>
                                                    <option value="between 100 to 500 milion">از 100 تا 500 میلیون تومان</option>
                                                    <option value="between 500 milion to 1 miliard">از 500 میلیون تا یک میلیارد تومان</option>
                                                    <option value="between 1 to 5 miliard">از 1 تا 5 میلیارد تومان</option>
                                                    <option value="between 5 to 10 miliard">از 5 تا 10 میلیارد توامن</option>
                                                    <option value="above 10 miliard">بیش از 10 میلیارد تومان</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="id_subject">میزان آشنایی شما با بازار بورس چقدر است</label>
                                                <select name="familiarity" value={familiarity}  onChange={this.handleOnChange} aria-required="true" class="form-control required name" required="" id="id_familiarity">
                                                    <option value="" selected="">---------</option>
                                                    <option value="low">کم</option>
                                                    <option value="Moderate">متوسط</option>
                                                    <option value="well">خوب</option>
                                                    <option value="high">عالی</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group center">
                                                <button class="btn" type="submit" id="form-submit"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;افتتاح حساب</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div className="col-lg-12">
                                        {successMessage  ? <Alerts />:'' }
                                    </div>
                                </div>
                            {/* </div>
                        </div> */}
                </AccountRegDiv>
            </React.Fragment>
        )
    }
}

export default AccountReg;