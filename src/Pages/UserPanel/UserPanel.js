import React, { Component } from 'react';
import Banner from '../../Components/Banner/Banner';

class UserPanel extends Component {
    render() {
        return (
            <React.Fragment>
            <Banner />
          
                 <section className="blog_area section-padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 mb-5 mb-lg-0">
                                <div className="col-lg-12">
                                    <h1 className="text-center">
                                        This is a USER PANEL
                                    </h1>
                                 </div>
                            </div>
                        
                        </div>
                    </div>
                </section>
            
            </React.Fragment>
        );
    }
}

export default UserPanel;
