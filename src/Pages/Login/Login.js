import React, { Component } from 'react';
import {GetinDiv} from './style';
import validator from 'validator';
import axios from 'axios';
import Banner from '../../Components/Banner/Banner';
// import {Redirect, Route} from "react-router-dom";
// import { browserHistory } from 'react-router';
// import axiosInstance from "../../axiosApi";



 class Login extends Component {
    constructor(props) {
        super(props);
        if(this.props.auth) {
             this.props.history.push('/')
        }

        this.state = {
            fields : {
                username : '',
                password : ''
            },
            errors : {}
        }
    }

    handleValidation(callback) {
        let { fields } = this.state;
        let errors = {};
        let formIsValid = true;

        // username
        if(validator.isEmpty(fields.username)) {
            formIsValid = false;
            errors["username"] = "فیلد نام کاربری نمیتواند خالی بماند";
        }

        // password
        if(validator.isEmpty(fields.password)) {
            formIsValid = false;
            errors["password"] = "فیلد پسورد نمیتواند خالی بماند";
        } else if(! validator.isLength(fields.password , { min : 6 , max : undefined})) {
            formIsValid = false;
            errors["password"] = "فرمت پسورد اشتباه است";
        }

        this.setState({ errors },() => {
            return callback(formIsValid)
        });

    }
    handleChange(event) {
        let fields = this.state.fields;
        let target = event.target;
        fields[target.name] = target.value;
        this.setState({fields});
    }

    handleRequest() {
        const { username , password } = this.state.fields;

        let data = new FormData();
        data.append('username' , username);
        data.append('password' , password);
        let formIsValid = true; 
        let errors = {};

    
    axios.post('http://192.168.1.27:8010/api/jwt/login/' , data)
    .then(response => {
        if(response.data.status === "created"){
            this.props.handleSuccessfulAuth(response.data);
        }
        localStorage.setItem('token', response.data.token);
        this.props.login();
        this.props.history.push('/')
    })
    .catch(error => {
        console.log(error);
        errors['username'] = "نام کاربری یا رمز عبور اشتباه است."
        formIsValid = false;
    })
}
    handleSubmit(event) {
        event.preventDefault();

        this.handleValidation((valid) => {
            if(valid) this.handleRequest()
        })
    }
     render() {
         const {username,password} = this.state.fields;
         const {errors} = this.state;
         return (
             <React.Fragment>
             <Banner />
            <GetinDiv>
                     <div  className="get_in_tauch_area">
                         <div className="container">
                             <div className="row justify-content-center">
                                 <div className="col-lg-6">
                                     <div className="section_title text-center mb-90">
                                         <h3 className="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">ورود  </h3>
                                         <p className="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">مخاطب در این حوزه فعالیت مناسبی را در شبکه های اجتماعی تلگرام و اینستاگرام دنبال میکند</p>
                                     </div>
                                 </div>
                             </div>
                             <div className="row justify-content-center">
                                 <div className="col-lg-8">
                                     <div className="touch_form">
                                         
                                         <form onSubmit={this.handleSubmit.bind(this)}>
                                             <div className="row">
                                                 <div className="col-md-12">
                                                     <div className="single_input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">
                                                         <input type="text" name="username" value={username} onChange={this.handleChange.bind(this)} placeholder="نام کاربری" style={{display :errors["username"] ? 'is-invalid' : ''}}/>
                                                         <span className="invalid-feedback rtl" style={{display :errors["username"] ? 'block' : 'none '}}>{errors["username"]}</span>
                                                     </div>
                                                 </div>
                                                 <div className="col-md-12">
                                                     <div className="single_input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                                                         <input type="password" name="password"  value={password} onChange={this.handleChange.bind(this)} placeholder="رمزعبور" style={{display :errors["password"] ? 'is-invalid' : ''}} />
                                                         <span className="invalid-feedback rtl" style={{display :errors["password"] ? 'block' : 'none '}}>{errors["password"]}</span>
                                                     </div>
                                                 </div>

                                                 <div className="col-lg-12">
                                                     <div className="submit_btn wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">
                                                         <button className="boxed-btn3" type="submit">ورود  </button>
                                                     </div>
                                                 </div>
                                             </div>
                                         </form>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </GetinDiv>
             </React.Fragment>
         )
     }
 }
 export default Login; 
