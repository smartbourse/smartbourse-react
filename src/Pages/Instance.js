import axios from "axios";

const instance = axios.create({
    baseURL: "https://smartbourse.com/api/v1/shop/order/",
    headers: {
        "Authorization": 'Bearer ' + localStorage.getItem('token'),
        "content-type": "application/json"
        },
    responseType: "json"
});
    
      
export default instance;