import React, { Component } from 'react'
import {SingleTutDiv } from './style';
import Banner from '../../Components/Banner/Banner';
import axios from 'axios';
import Search from '../../Components/Search/Search';
import Category from '../../Components/Category/Category';
import Recently from '../../Components/Recently/Recently';

class SingleTutorial extends Component {

    constructor(props) {
        super(props); 
        this.state = {
            article :[],
            dataCategories:[],
            recently:[]
        }
    }
    
    componentDidMount() {
        const { params } = this.props.match;
        console.log(params);
        
        axios.get(`https://smartbourse.com/api/v1/analysis/${params.slug}`)
        .then(response => {
            const data = response.data;
            
            this.setState({
                article: data
            })

            console.log('Look here:', this.state.article);
        })
        .catch(error => {
            console.log(error);
        })


        axios.get('https://smartbourse.com/api/v1/news/category/')
        .then(response => {
            const dataCategory = response.data.results;  
            this.setState({
                dataCategories: dataCategory
            })

            console.log('Look here category:', this.state.dataCategories);
        })
        .catch(error => {
            console.log(error);
        })


        axios.get('https://smartbourse.com/api/v1/news/recently/')
        .then(response => {
            const dataRecently = response.data;  
            this.setState({
                recently: dataRecently
            })

            console.log('Look here recently:', this.state.recently);
        })
        .catch(error => {
            console.log(error);
        })
    }
    

    render() {
        const { article } = this.state;
        return (
            <React.Fragment>
            <Banner />
            <SingleTutDiv>
            <section className="blog_area single-post-area section-padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-4">
                                <div className="blog_right_sidebar">
                                    <Search />
                                    <aside className="single_sidebar_widget post_category_widget">
                                        <h4 className="widget_title">دسته بندی مقالات</h4>
                                        <ul className="list cat-list">
                                            {this.state.dataCategories.map((post , index) => <Category title={post.title} post={post} key={index}/>)} 
                                        </ul>
                                    </aside>
                                    <aside className="single_sidebar_widget popular_post_widget">
                                        <h3 className="widget_title">تحلیل های اخیر</h3> 
                                        {this.state.recently.map((post , index) => <Recently  post={post} key={index}/>)}
                                    </aside>
                                </div>
                            </div>                     
                            <div className="col-lg-8 posts-list">
                            {/* <Content obj={this.state.singleNews} /> */}          
                            <div className="single-post">
                                <div className="feature-img">
                                <img className="img-fluid"  alt="" />
                                </div>
                                <div className="ck_beautify">
                                    <h2>                      
                                    {article.title}
                                    </h2>
                                    <div dangerouslySetInnerHTML={{ __html: article.content }} />
                                </div> 
                            </div>                             
                            </div>
                        </div>
                    </div>
                </section>
                </SingleTutDiv>
                </React.Fragment>
        )
    }
}

export default SingleTutorial;