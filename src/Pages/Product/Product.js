import React, { Component } from 'react';
import Slider from '../../Components/Slider/Slider';
import { Productdiv } from './style';
import axios from 'axios';
import Modals from '../../Components/Modal/Modal';
import instance from '../Instance';
// import primage from '../shop/pr.jpg';

;

class Product extends Component {
    constructor(props) {
        super(props); 
        
        this.state = {
            products :[],
            isvalid : "",
        }
        this.add = this.add.bind(this);
    }

  


    add(slug, title) {

        console.log("Haya")
        axios.post("https://smartbourse.com/api/v1/shop/order/", {
            headers: {
                "Authorization": 'Bearer ' + localStorage.getItem('token'),
                "content-type": "application/json"
                },
            responseType: "json"
        })
    }

    componentDidMount() {
        const { params } = this.props.match;
        console.log(params);
        
        axios.get(`https://smartbourse.com/api/v1/products/${params.slug}`)
        .then(response => {
            const data = response.data;
            
            this.setState({
                products: data
            })

            console.log('Look product:', this.state.products);
        })
        .catch(error => {
            console.log(error);
        })
    }
    render() {
        const { products } = this.state;
        return (
            <React.Fragment>
                <Slider />
                <Productdiv className="container">
                    <br />

                    <div className="row">

                        <div className="col-md-6 pp-image"><img className="product-image" alt="smart" /></div>
                        <div className="col-md-6 pp-info">
                            <h3>  {products.title} </h3>
                            <hr />
                            <h5>توضیحات:</h5>
                            <p className="product-description">  <div dangerouslySetInnerHTML={{ __html: products.content }} /></p>
                            <div className="row my-4">
                                <div className="col-md-4 product-id text-center"><p>کد کالا : <span>0001</span></p></div>

                                <div className="col-md-4 add-to-card text-center"> 
                                    <button key={products.slug} onClick={() => this.add(products.slug, products.title)}> افزودن به سبد </button> 
                                </div>

                                <div className="col-md-4 QNT text-center">تعداد:<input type="number" defaultValue="50" min="0" max="100" step="1"/></div>

                            </div>
                            <div>
                        {/* <div>{ console.log(this.state.isvalid) }</div> */}
                            {/* {this.state.isvalid ? <h1>hiiii</h1> : <Modals/> } */}
                        </div>
                            <div className="container">

                                <ul className="nav nav-tabs product-multi-info py-2">
                                    <li className="px-0 col-md-3 text-center"><a className="py-2 px-3  show active" data-toggle="tab" href="#menu1">مشخصات</a></li>
                                    <li className="px-0 col-md-3 text-center"><a className="py-2 px-3" data-toggle="tab" href="#menu2">نظرات کاربران</a></li>
                                    <li className="px-0 col-md-3 text-center"><a className="py-2 px-3" data-toggle="tab" href="#menu3">پرسش و پاسخ</a></li>
                                    <li className="px-0 col-md-3 text-center"><a className="py-2 px-3" data-toggle="tab" href="#home">نقد و بررسی</a></li>
                                </ul>

                                <div className="tab-content product-multi-content px-2 py-3">
                                    <div id="home" className="tab-pane fade in">
                                        <h5 className="my-2">نقد و بررسی</h5>
                                        <p className="p-2">راحان گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، </p>
                                    </div>
                                    <div id="menu1" className="tab-pane fade show active">
                                        <h5 className="my-2">مشخصات</h5>
                                        <p className="p-2">راحان گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، </p>
                                    </div>
                                    <div id="menu2" className="tab-pane fade">
                                        <h5 className="my-2">نظرات کاربران</h5>
                                        <p className="p-2">راحان گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، </p>
                                    </div>
                                    <div id="menu3" className="tab-pane fade">
                                        <h5 className="my-2">پرسش و پاسخ</h5>
                                        <p className="p-2">راحان گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، چاپگرها و متون با استفاده از طراحان گرافیک گرافیک است، </p>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>



                </Productdiv>



            </React.Fragment>
        );
    }
}
export default Product;
