import React, { Component } from 'react';
import {TutorialDiv } from './style';
import Banner from '../../Components/Banner/Banner';
import axios from 'axios';
import Blog from '../../Components/Blog/Blog';
import Category from '../../Components/Category/Category';
import Recently from '../../Components/Recently/Recently';

class Tutorial extends Component {
    // static defaultProps = {
    //     name: 'آموزش'
    //   }

      constructor(props) {
        super(props);
        this.state = {
            articles : [],
            dataCategories:[],
            recently:[]
        }
    }

    componentDidMount() {
        axios.get("https://smartbourse.com/api/v1/tutorial/")
            .then(response => {
                const data = response.data.results;               
                this.setState({
                    articles: data
                })

                console.log('Look here:', this.state.articles);
            })
            .catch(error => {
                console.log(error);
            })

            axios.get('https://smartbourse.com/api/v1/tutorial/category/')
            .then(response => {
                const dataCategory = response.data.results;  
                this.setState({
                    dataCategories: dataCategory
                })
    
                console.log('Look here category:', this.state.dataCategories);
            })
            .catch(error => {
                console.log(error);
            })

            axios.get('https://smartbourse.com/api/v1/tutorial/recently/')
            .then(response => {
                const dataRecently = response.data;  
                this.setState({
                    recently: dataRecently
                })
    
                console.log('Look here recently:', this.state.recently);
            })
            .catch(error => {
                console.log(error);
            })
        }


    render() { 
        return (
            <React.Fragment>
            <Banner  name={this.props.default}/>
            <TutorialDiv>
                 <section className="blog_area section-padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-4">
                                <div className="blog_right_sidebar">
                                    <aside className="single_sidebar_widget search_widget">
                                        <form action="#">
                                            <div className="form-group">
                                                <div className="input-group mb-3">
                                                    <input type="text" className="form-control" placeholder='بنویس، تا کمکت کنم...' />
                                                    <div className="input-group-append">
                                                        <button className="btn" type="button"><i className="ti-search"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <button className="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn"
                                                type="submit">جستوجو</button>
                                        </form>
                                    </aside>

                                    <aside className="single_sidebar_widget post_category_widget">
                                        <h4 className="widget_title">دسته بندی مقالات</h4>
                                        <ul className="list cat-list">
                                            {this.state.dataCategories.map((post , index) => <Category title={post.title} post={post} key={index}/>)} 
                                        </ul>
                                    </aside>

                                    <aside className="single_sidebar_widget popular_post_widget">
                                        <h3 className="widget_title">تحلیل های اخیر</h3> 
                                        {this.state.recently.map((post , index) => <Recently  post={post} key={index}/>)}
                                    </aside>

                                    {/* <aside className="single_sidebar_widget instagram_feeds">
                                        <h4 className="widget_title">پست های اخیر اینستاگرام</h4>
                                        <ul className="instagram_row flex-wrap">
                                            <li>
                                                <a href="#">
                                                    <img className="img-fluid" src="img/post/post_5.png" alt="" />
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img className="img-fluid" src="img/post/post_6.png" alt="" />
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img className="img-fluid" src="img/post/post_7.png" alt="" />
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img className="img-fluid" src="img/post/post_8.png" alt="" />
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img className="img-fluid" src="img/post/post_9.png" alt="" />
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img className="img-fluid" src="img/post/post_10.png" alt="" />
                                                </a>
                                            </li>
                                        </ul>
                                    </aside> */}

{/* 
                                    <aside className="single_sidebar_widget newsletter_widget">
                                        <h4 className="widget_title">با ما در تماس باشید</h4>

                                        <form action="#">
                                            <div className="form-group">
                                                <input type="email" className="form-control" 
                                                    placeholder='شماره تماس خود را وارد کنید' required />
                                            </div>
                                            <button className="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn"
                                                type="submit">ارسال</button>
                                        </form>
                                    </aside> */}
                                </div>
                            </div>                     
                            <div className="col-lg-8 mb-5 mb-lg-0">
                                <div className="col-lg-12">
                                    {this.state.articles.map((post , index) => <Blog page_path="tutorial" col="col-lg-6" post={post} key={index}/>)} 
                                </div>
                            </div>
                                {/* <nav className="blog-pagination justify-content-center d-flex">
                                        <ul className="pagination">
                                            <li className="page-item">
                                                <a href="#" className="page-link" aria-label="Previous">
                                                    <i className="ti-angle-left"></i>
                                                </a>
                                            </li>
                                            <li className="page-item">
                                                <a href="#" className="page-link">1</a>
                                            </li>
                                            <li className="page-item active">
                                                <a href="#" className="page-link">2</a>
                                            </li>
                                            <li className="page-item">
                                                <a href="#" className="page-link" aria-label="Next">
                                                    <i className="ti-angle-right"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav> */}
                        
                        </div>
                    </div>
                </section>
            </TutorialDiv>
            </React.Fragment>
        )
    }
}
export default Tutorial;