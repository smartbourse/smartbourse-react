import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {TutorialDiv } from './style';



class Tut extends Component {

    render() {
        const {post} = this.props;
        return (
            <TutorialDiv>  
                <div className="col-lg-6" style={{float:"right"}}>
                    <div className="blog_left_sidebar">
                        <div className="blog_item">
                            <div className="blog_item_img">
                                <img className="card-img rounded-0" src={post.picture} alt="" />
                                <Link to={`${this.props.page_path}/${post.slug}`} className="blog_item_date">
                                    <h3>15 </h3>
                                    <p>فروردین</p>
                                </Link>
                            </div>
                            <div className="blog_details">
                                    <Link to={`${this.props.page_path}/${post.slug}`} className="d-inline-block"> 
                                    <h2>{post.title}</h2>
                                    </Link>
                                <p>{post.summary.substr(0,200)}...</p>
                                <ul className="blog-info-link">
                                    <li><Link to={`${this.props.page_path}/${post.slug}`}><i className="fa fa-angle-left"></i> بیشتر بدانید ...</Link></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </TutorialDiv>
            )
        }
    }
export default Tut; 
