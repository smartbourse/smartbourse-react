import React, { Component } from 'react';
import Banner from '../../Components/Banner/Banner';
import { Firstdiv ,ArrowSVG } from './style'
import { Accordion ,Card } from 'react-bootstrap';
import axios from 'axios';
// import CardPrimary from '../../Components/CardPrimary/CardPrimary';


class FirstPB extends Component {

    constructor(props) {
        super(props);
        this.state = {
            articles : [],

        }
    }

    componentDidMount() {
        axios.get("https://smartbourse.com/api/v1/primarymarket/?format=json")
            .then(response => {
                const data = response.data.results;               
                this.setState({
                    articles: data
                })

                console.log('Look here:', this.state.articles);
            })
            .catch(error => {
                console.log(error);
            })


        }


    render() {

        // const {post} = this.props;

        return (
 
            <React.Fragment>
            <Banner />
                <Firstdiv>
                    <h1>عرضه اولیه چیست؟</h1>
                    <p>عرضه اولیه به سهام هایی گفته میشود که برای اولین بار در بورس و فرابورص به صورت عمومه عرضه شده و در دسترس مردم برای خرید قرار گرفته است.</p>
                    <h2>لیست عرضه اولیه های آتی</h2>

                    <Accordion >
                    {this.state.articles.map((post , index) => 
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey={post.abbrivation}>
                        {post.title}
                                <ArrowSVG>
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-up" id="Arroww" className=" svg-inline--fa fa-chevron-up fa-w-14 arrowte" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M240.971 130.524l194.343 194.343c9.373 9.373 9.373 24.569 0 33.941l-22.667 22.667c-9.357 9.357-24.522 9.375-33.901.04L224 227.495 69.255 381.516c-9.379 9.335-24.544 9.317-33.901-.04l-22.667-22.667c-9.373-9.373-9.373-24.569 0-33.941L207.03 130.525c9.372-9.373 24.568-9.373 33.941-.001z"></path></svg>  
                                </ArrowSVG>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey={post.abbrivation}>
                            <Card.Body><p dangerouslySetInnerHTML={{ __html: post.content }} /></Card.Body>
                                
                            </Accordion.Collapse>
                        </Card>
                        )} 

                    </Accordion>

                </Firstdiv>


            </React.Fragment>
        );
    }
}






export default FirstPB;